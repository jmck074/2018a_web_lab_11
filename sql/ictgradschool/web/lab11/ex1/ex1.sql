-- Answers to exercise 1 questions

SELECT DISTINCT dept
FROM unidb_courses;

SELECT DISTINCT semester
FROM unidb_attend;

SELECT DISTINCT dept, num
FROM unidb_attend;

SELECT fname, lname, country
FROM unidb_students
ORDER BY fname;

SELECT fname, lname, mentor
FROM unidb_students
ORDER BY mentor;

SELECT fname, lname
FROM unidb_lecturers
ORDER BY office;

SELECT fname, lname
FROM unidb_lecturers
WHERE staff_no>500;

SELECT fname, lname, id
FROM unidb_students
WHERE 1668<id AND id<1824;

SELECT fname, lname, country
FROM unidb_students
WHERE country IN ('NZ', 'AU', 'US');

SELECT fname, lname, office
FROM unidb_lecturers
WHERE office LIKE 'G%';

SELECT dept, num
FROM unidb_courses
WHERE dept != 'comp';

SELECT fname, lname
FROM unidb_students
WHERE country IN ('FR', 'MX');




