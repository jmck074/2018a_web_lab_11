-- Answers to exercise 2 questions
SELECT fname, lname, dept, num
FROM unidb_students, unidb_attend
WHERE unidb_students.id = unidb_attend.id
AND dept='comp' AND num='219';

SELECT fname, lname, country
FROM unidb_students AS s, unidb_courses AS c
WHERE country != 'NZ' AND s.id=c.rep_id;

SELECT DISTINCT office
FROM unidb_lecturers l, unidb_teach t
WHERE l.staff_no= t.staff_no AND t.num='219';

SELECT DISTINCT office
FROM unidb_lecturers l JOIN unidb_teach t
ON l.staff_no= t.staff_no AND t.num='219';

SELECT DISTINCT s.fname, s.lname
FROM unidb_students AS s, unidb_attend AS a, unidb_lecturers AS l, unidb_teach AS t
WHERE l.fname="Te Taka" AND l.staff_no = t.staff_no AND t.dept=a.dept AND t.num=a.num AND a.id = s.id;

SELECT DISTINCT s.fname, s.lname
FROM unidb_students AS  s
JOIN unidb_attend AS a ON s.id = a.id
JOIN unidb_teach AS t ON t.dept = a.dept AND t.num=a.num
JOIN unidb_lecturers AS l ON l.staff_no=t.staff_no
WHERE l.fname="Te Taka";

SELECT DISTINCT s.fname, s.lname, m.fname, m.lname
FROM unidb_students AS s
JOIN unidb_students AS m ON s.mentor= m.id;

SELECT fname, lname
FROM unidb_lecturers
WHERE office LIKE ('G%')
UNION
SELECT fname, lname
FROM unidb_students
WHERE country !='NZ';

SELECT cc.fname, cc.lname, sr.fname, sr.lname
FROM unidb_courses AS course
  JOIN unidb_students AS sr ON course.rep_id=sr.id
  JOIN unidb_lecturers AS cc ON course.coord_no=cc.staff_no
WHERE course.dept='comp' AND course.num='219';


